﻿
namespace DI_XML_Empleados2
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbIDEmpleado = new System.Windows.Forms.ComboBox();
            this.cbNombreEmpleado = new System.Windows.Forms.ComboBox();
            this.tbResultado = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cbIDEmpleado
            // 
            this.cbIDEmpleado.FormattingEnabled = true;
            this.cbIDEmpleado.Location = new System.Drawing.Point(47, 84);
            this.cbIDEmpleado.Name = "cbIDEmpleado";
            this.cbIDEmpleado.Size = new System.Drawing.Size(169, 21);
            this.cbIDEmpleado.TabIndex = 0;
            this.cbIDEmpleado.SelectedIndexChanged += new System.EventHandler(this.cbIDEmpleado_SelectedIndexChanged);
            // 
            // cbNombreEmpleado
            // 
            this.cbNombreEmpleado.FormattingEnabled = true;
            this.cbNombreEmpleado.Location = new System.Drawing.Point(50, 191);
            this.cbNombreEmpleado.Name = "cbNombreEmpleado";
            this.cbNombreEmpleado.Size = new System.Drawing.Size(169, 21);
            this.cbNombreEmpleado.TabIndex = 1;
            this.cbNombreEmpleado.SelectedIndexChanged += new System.EventHandler(this.cbNombreEmpleado_SelectedIndexChanged);
            // 
            // tbResultado
            // 
            this.tbResultado.Location = new System.Drawing.Point(276, 54);
            this.tbResultado.Multiline = true;
            this.tbResultado.Name = "tbResultado";
            this.tbResultado.Size = new System.Drawing.Size(308, 209);
            this.tbResultado.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "búsqueda por ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 164);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "búsqueda por Nombre";
            // 
            // lbInfo
            // 
            this.lbInfo.AutoSize = true;
            this.lbInfo.Location = new System.Drawing.Point(47, 287);
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.Size = new System.Drawing.Size(0, 13);
            this.lbInfo.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(616, 330);
            this.Controls.Add(this.lbInfo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbResultado);
            this.Controls.Add(this.cbNombreEmpleado);
            this.Controls.Add(this.cbIDEmpleado);
            this.Name = "Form1";
            this.Text = "BÚSQUEDA XML";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbIDEmpleado;
        private System.Windows.Forms.ComboBox cbNombreEmpleado;
        private System.Windows.Forms.TextBox tbResultado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbInfo;
    }
}

