﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.XPath;

namespace DI_XML_Empleados2
{
    public partial class Form1 : Form
    {
        private XmlReader reader;
        
        //Constructor
        public Form1()
        {            
            InitializeComponent();
            this.reader = XmlReader.Create("empleados.xml");
            rellenarComboBox();
            lbInfo.Text = "";
        }




        /// <summary>
        /// Rellena ambos comboBox
        /// </summary>
        private void rellenarComboBox()
        {
            cbIDEmpleado.Text = "ID Empleados";
            cbNombreEmpleado.Text = "Nombre Empleados";
            String nombreCompletoEmpleado = "";

            while (this.reader.Read())
            {
                //Rellenamos el Combobox de los 'ID'
                if (this.reader.Name.ToString() == "idEmpleado")
                {
                    cbIDEmpleado.Items.Add(this.reader.ReadString());
                }


                //Rellenamos el Combobox del 'nombre' y los 'apellidos'
                if (this.reader.Name.ToString() == "nombre")
                    nombreCompletoEmpleado += this.reader.ReadString() + " ";

                if (this.reader.Name.ToString() == "apellidos")
                {
                    nombreCompletoEmpleado += this.reader.ReadString();
                    cbNombreEmpleado.Items.Add(nombreCompletoEmpleado);
                    nombreCompletoEmpleado = "";
                }
            }
        }

        //Mostramos los datos del empleado seleccionado a partir de su 'ID'
        private void cbIDEmpleado_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbResultado.Text = "";

            XmlDocument doc = new XmlDocument();
            doc.Load("empleados.xml");
            String strExpres = "/empleados/empleado[idEmpleado='" + cbIDEmpleado.SelectedItem.ToString() + "']";

            XmlNodeList nodeList = doc.SelectNodes(strExpres);

            foreach (XmlNode node in nodeList)
            {
                XmlElement element = (XmlElement)node;
                tbResultado.Text += element.GetElementsByTagName("idEmpleado")[0].Name + " " + element.GetElementsByTagName("idEmpleado")[0].InnerText + "\r\n" 
                    + element.GetElementsByTagName("nombre")[0].Name + " " + element.GetElementsByTagName("nombre")[0].InnerText + "\r\n" +
                    element.GetElementsByTagName("apellidos")[0].Name + " " +  element.GetElementsByTagName("apellidos")[0].InnerText + "\r\n"
                    + element.GetElementsByTagName("numeroSS")[0].Name + " " +  element.GetElementsByTagName("numeroSS")[0].InnerText + "\r\n" +
                    element.GetElementsByTagName("domicilio")[0].Name + " " +  element.GetElementsByTagName("domicilio")[0].InnerText + "\r\n"
                    + element.GetElementsByTagName("telefonos")[0].ChildNodes[0].Name + " " +  element.GetElementsByTagName("telefonos")[0].ChildNodes[0].InnerText + "\r\n" +
                    element.GetElementsByTagName("telefonos")[0].ChildNodes[1].Name + " " +  element.GetElementsByTagName("telefonos")[0].ChildNodes[1].InnerText;
            }
        }

        //Mostraos los datos del empleado seleccionado a partir de su 'nombre' y 'apellidos'
        private void cbNombreEmpleado_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbResultado.Text = "";
            //Obtenemos primero el nombre del empleado y de su apellido
            String[] nombreCompletoEmpleado = cbNombreEmpleado.SelectedItem.ToString().Split(' ');

            String nombreEmpleado = nombreCompletoEmpleado[0];
            String apellidosEmpleado = nombreCompletoEmpleado[1] +" "+ nombreCompletoEmpleado[2];

            XmlDocument doc = new XmlDocument();
            doc.Load("empleados.xml");
            String strExpres = "/empleados/empleado[nombre='" + nombreEmpleado + "' and apellidos='" + apellidosEmpleado + "']";

            XmlNodeList nodeList = doc.SelectNodes(strExpres);

            //Para comprobar si la expresión es correcta
            //int count1 = nodeList.Count;

            foreach (XmlNode node in nodeList)
            {
                XmlElement element = (XmlElement)node;
                tbResultado.Text += element.GetElementsByTagName("idEmpleado")[0].Name + " " + element.GetElementsByTagName("idEmpleado")[0].InnerText + "\r\n"
                    + element.GetElementsByTagName("nombre")[0].Name + " " + element.GetElementsByTagName("nombre")[0].InnerText + "\r\n" +
                    element.GetElementsByTagName("apellidos")[0].Name + " " + element.GetElementsByTagName("apellidos")[0].InnerText + "\r\n"
                    + element.GetElementsByTagName("numeroSS")[0].Name + " " + element.GetElementsByTagName("numeroSS")[0].InnerText + "\r\n" +
                    element.GetElementsByTagName("domicilio")[0].Name + " " + element.GetElementsByTagName("domicilio")[0].InnerText + "\r\n"
                    + element.GetElementsByTagName("telefonos")[0].ChildNodes[0].Name + " " + element.GetElementsByTagName("telefonos")[0].ChildNodes[0].InnerText + "\r\n" +
                    element.GetElementsByTagName("telefonos")[0].ChildNodes[1].Name + " " + element.GetElementsByTagName("telefonos")[0].ChildNodes[1].InnerText;
            }
        }
    }
}
