﻿using System;
using System.Xml;

namespace DI_XML1
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlReader reader = XmlReader.Create("empleados.xml");

            Console.WriteLine("Leemos el archivo 'empleados.xml' \n");

            //Leemos
            while (reader.Read())
            {
                if(reader.Name.ToString() == "nombre")
                {
                    Console.Write("Nombre: ");
                    Console.Write(reader.ReadString());
                }
               
                if (reader.Name.ToString() == "apellidos")
                    Console.Write(reader.ReadString() + "\n");
            }            
            Console.ReadLine();
        }
    }
}
